# CS371p: Object-Oriented Programming Collatz Repo

* Name: (your Full Name)
Pranav Srinivasan

* EID: (your EID)
ss79767

* GitLab ID: (your GitLab ID)
15792926

* HackerRank ID: (your HackerRank ID)
@srinivasan_pran1

* Git SHA: bc7e9168c29bf42f7bd608ef80d66787542bd1b1

* GitLab Pipelines: https://gitlab.com/psrini/cs371p-collatz/-/pipelines

* Estimated completion time: 8

* Actual completion time: 12

* Comments: (any additional comments you have)

Some commit messages do not directly address an issue because they were a smaller fix. Between the commit messages that do address issues, all un-mentioned issues were addressed in the intermittent commits. Additionally, some of the issues that were created during development(not the 10 from the workflow) were created from bugs
that were also resolved quickly, so those issues closed quickly as well.
