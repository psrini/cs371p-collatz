// ----------------
// test_Collatz.cpp
// ----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// --------------
// CollatzFixture
// --------------

TEST(CollatzFixture, collatz_eval_0) {
    const tuple_type_1 t1 = {1, 10};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1, 10, 20};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_1) {
    const tuple_type_1 t1 = {100, 200};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {100, 200, 125};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_2) {
    const tuple_type_1 t1 = {201, 210};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {201, 210, 89};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_3) {
    const tuple_type_1 t1 = {900, 1000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {900, 1000, 174};
    ASSERT_TRUE(t2 == t3);
}

//Unit Tests

TEST(CollatzFixture, collatz_eval_4) {
    const tuple_type_1 t1 = {257846, 491457};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {257846, 491457, 449};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_5) {
    const tuple_type_1 t1 = {558992, 283089};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {558992, 283089, 470};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_6) {
    const tuple_type_1 t1 = {732287, 717133};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {732287, 717133, 411};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_7) {
    const tuple_type_1 t1 = {435844, 893733};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {435844, 893733, 525};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_8) {
    const tuple_type_1 t1 = {733900, 167643};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {733900, 167643, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_9) {
    const tuple_type_1 t1 = {609629, 130747};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {609629, 130747, 470};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_10) {
    const tuple_type_1 t1 = {760444, 351270};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {760444, 351270, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_11) {
    const tuple_type_1 t1 = {312565, 442623};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {312565, 442623, 449};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_12) {
    const tuple_type_1 t1 = {537425, 564298};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {537425, 564298, 452};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_13) {
    const tuple_type_1 t1 = {707234, 517041};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {707234, 517041, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_14) {
    const tuple_type_1 t1 = {992398, 946458};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {992398, 946458, 458};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_15) {
    const tuple_type_1 t1 = {764631, 228526};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {764631, 228526, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_16) {
    const tuple_type_1 t1 = {733706, 296661};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {733706, 296661, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_17) {
    const tuple_type_1 t1 = {707558, 198057};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {707558, 198057, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_18) {
    const tuple_type_1 t1 = {634612, 312067};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {634612, 312067, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_19) {
    const tuple_type_1 t1 = {901153, 32543};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {901153, 32543, 525};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_20) {
    const tuple_type_1 t1 = {776711, 77168};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {776711, 77168, 509};
    ASSERT_TRUE(t2 == t3);
}
