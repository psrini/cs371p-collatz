// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"
using namespace std;
#include <algorithm>
#include <iostream>
long long cache[1000000];

// ------------
// collatz_eval
// ------------

//precondition

tuple_type_2 collatz_eval (const tuple_type_1& t1) {

    auto [a, b] = t1;
    int i = a;
    int j = b;

    //maintaining an order of arguments
    if(a > b) {
        i = b;
        j = a;
    } else {
        i = a;
        j = b;
    }


    //precondition inputs are integers
    assert(i == (int)i);
    assert(j == (int)j);

    //argument validity check
    assert(i > 0);
    assert(j > 0);
    //int v = i + j; // fix!
    int max_ans = 1;

    //half-range optimization
    long long m = j/2 + 1;
    if(i < m) i = m;

    for(int x = i; x <= j; x++) {
        //keep track of original index since we mutate it
        //make it a long due to mutation
        long long temp = x;

        //cout << x;

        //basic Collatz loop, make sure count is 1
        int count = 1;
        if(cache[x] > 0) {
            count = cache[x];
        } else {
            while(temp > 1) {
                if(temp % 2 == 0)
                    temp /= 2;
                else {
                    temp += (temp >> 1) + 1;
                    count++;
                }
                count++;
            }
            cache[x] = count;
        }
        max_ans = max(count, max_ans);
    }
    //return value validity
    //cout << max_ans;
    assert(max_ans > 0);

    //Post condition:
    //make sure that inputs weren't modified
    assert(i > 0);
    assert(j > 0);
    //post-cond
    return {a, b, max_ans};
}
